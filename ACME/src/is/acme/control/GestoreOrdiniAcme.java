package is.acme.control;

import java.util.ArrayList;

import is.acme.entity.*;



public class GestoreOrdiniAcme {

	int N = 2; 

	private ArrayList<Cliente> listaClienti; 


	public GestoreOrdiniAcme() {	

		listaClienti = new ArrayList<Cliente>(); 

	}

	public void aggiungiCliente(Cliente c) {

		listaClienti.add(c); 

	}


	public void aggiungiSpesa(Cliente c, String id, double costoTotale) {


		Spesa s = new Spesa(id, costoTotale);  

		c.getListaSpesa().add(s); 


	}


	public void generaReport() {

		Boolean numMin = true; 

		if(listaClienti.size()>0) {

			System.out.println("---Report---- \n"); 		

			for (Cliente c : listaClienti) { 		      

				if(c.getListaSpesa().size()>=N) {

					double costoComplessivo = 0; 

					for (Spesa s : c.getListaSpesa()) {

						costoComplessivo += s.getCostoTotale(); 

					}

					System.out.println("Cliente: " + c.getNomeUtente() + " ; Numero Spese: " + c.getListaSpesa().size() + " ; Costo Complessivo: " + costoComplessivo); 

				}else{ numMin = false ; }

			}
			if (numMin == false) {System.out.println("Spiacente, non ci sono clienti che hanno effettuato almeno N acquisti."); }

		}else { System.out.println("Spiacente, non ci sono clienti"); }

	}



	public ArrayList<Cliente> getListaClienti() {
		return listaClienti;
	}


	public void setListaClienti(ArrayList<Cliente> listaClienti) {
		this.listaClienti = listaClienti;
	}




}
