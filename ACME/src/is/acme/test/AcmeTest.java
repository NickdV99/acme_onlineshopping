package is.acme.test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import is.acme.control.GestoreOrdiniAcme;
import is.acme.entity.Cliente;

public class AcmeTest {

	private GestoreOrdiniAcme gestoreOrdini = new GestoreOrdiniAcme();
	
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test01() {
	
		Cliente c1 = new Cliente("Nicola");  
		gestoreOrdini.aggiungiCliente(c1);
		assertEquals(1,gestoreOrdini.getListaClienti().size()); 
		
	}
	
	@Test
	public void test02() {
		
		Cliente c1 = new Cliente("Nicola");  
		Cliente c2 = new Cliente("Luigi");  
		gestoreOrdini.aggiungiCliente(c1);
		gestoreOrdini.aggiungiCliente(c2);
		assertEquals(2,gestoreOrdini.getListaClienti().size()); 
		
		
		
	}
	
	@Test
	public void test03() {
		
		Cliente c1 = new Cliente("Nicola");  
		gestoreOrdini.aggiungiCliente(c1);
		assertEquals(0,c1.getListaSpesa().size()); 
		
	}
	
	
	
	@Test
	public void test04() {
		
		Cliente c1 = new Cliente("Nicola");  
		gestoreOrdini.aggiungiCliente(c1);
		gestoreOrdini.aggiungiSpesa(c1, "S01", 20.5);
		assertEquals(1,c1.getListaSpesa().size()); 
		
	}
	
	
	@Test
	public void test05() {
		
		Cliente c1 = new Cliente("Nicola");  
		gestoreOrdini.aggiungiCliente(c1);
		gestoreOrdini.aggiungiSpesa(c1, "S01", 20.5);
		gestoreOrdini.aggiungiSpesa(c1, "S02", 30.7);
		assertEquals(2,c1.getListaSpesa().size()); 
		
	}
	
	
	
	@Test
	public void test06() {
		
		Cliente c1 = new Cliente("Nicola");  
		gestoreOrdini.aggiungiCliente(c1);
		gestoreOrdini.aggiungiSpesa(c1, "S01", 20.5);
		gestoreOrdini.aggiungiSpesa(c1, "S02", 30.7);
		gestoreOrdini.aggiungiSpesa(c1, "S03", 2);
		assertEquals(3,c1.getListaSpesa().size()); 
		
	}

	
	@Test
	public void test07() {
		
		gestoreOrdini.generaReport(); 
		
	}
	
	@Test
	public void test08() {
		
		
		Cliente c1 = new Cliente("Nicola");  
		gestoreOrdini.aggiungiCliente(c1);
		gestoreOrdini.generaReport(); 
		
	}
	
	
	@Test
	public void test09() {
		
		
		Cliente c1 = new Cliente("Nicola");  
		gestoreOrdini.aggiungiCliente(c1);
		gestoreOrdini.aggiungiSpesa(c1, "S01", 20.5);
		gestoreOrdini.generaReport(); 
		
	}
	
	
	@Test
	public void test10() {
		
		
		Cliente c1 = new Cliente("Nicola");  
		gestoreOrdini.aggiungiCliente(c1);
		gestoreOrdini.aggiungiSpesa(c1, "S01", 20.5);
		gestoreOrdini.aggiungiSpesa(c1, "S02", 30.7);
		gestoreOrdini.generaReport(); 
		
	}
	
	@Test
	public void test11() {
		
		
		Cliente c1 = new Cliente("Nicola");  
		gestoreOrdini.aggiungiCliente(c1);
		gestoreOrdini.aggiungiSpesa(c1, "S01", 20.5);
		gestoreOrdini.aggiungiSpesa(c1, "S02", 30.7);
		gestoreOrdini.aggiungiSpesa(c1, "S03", 4);
		gestoreOrdini.generaReport(); 
		
	}
	
	@Test
	public void test12() {
		
		
		Cliente c1 = new Cliente("Nicola");
		Cliente c2 = new Cliente("Luigi");  
		gestoreOrdini.aggiungiCliente(c1);
		gestoreOrdini.aggiungiCliente(c2);
		gestoreOrdini.generaReport(); 
		
	}
	
	@Test
	public void test13() {
		
		
		Cliente c1 = new Cliente("Nicola");
		Cliente c2 = new Cliente("Luigi");  
		gestoreOrdini.aggiungiCliente(c1);
		gestoreOrdini.aggiungiCliente(c2);
		gestoreOrdini.aggiungiSpesa(c1, "S01", 20.5);
		gestoreOrdini.aggiungiSpesa(c2, "S02", 30.7);
		gestoreOrdini.generaReport(); 
		
	}
	
	@Test
	public void test14() {
		
		
		Cliente c1 = new Cliente("Nicola");
		Cliente c2 = new Cliente("Luigi");  
		gestoreOrdini.aggiungiCliente(c1);
		gestoreOrdini.aggiungiCliente(c2);
		gestoreOrdini.aggiungiSpesa(c1, "S01", 2);
		gestoreOrdini.aggiungiSpesa(c1, "S02", 3);
		gestoreOrdini.aggiungiSpesa(c2, "S03", 10);
		gestoreOrdini.aggiungiSpesa(c2, "S04", 20);
		gestoreOrdini.generaReport(); 
		
	}
	
	@Test
	public void test15() {
		
		
		Cliente c1 = new Cliente("Nicola");
		Cliente c2 = new Cliente("Luigi");  
		gestoreOrdini.aggiungiCliente(c1);
		gestoreOrdini.aggiungiCliente(c2);
		gestoreOrdini.aggiungiSpesa(c1, "S01", 2);
		gestoreOrdini.aggiungiSpesa(c1, "S02", 3);
		gestoreOrdini.aggiungiSpesa(c1, "S05", 4);
		gestoreOrdini.aggiungiSpesa(c2, "S03", 10);
		gestoreOrdini.aggiungiSpesa(c2, "S04", 20);
		gestoreOrdini.aggiungiSpesa(c2, "S06", 30);
		gestoreOrdini.generaReport(); 
		
	}
	
	
	
}
