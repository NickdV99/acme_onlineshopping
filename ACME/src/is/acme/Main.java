package is.acme;

import is.acme.control.GestoreOrdiniAcme;
import is.acme.entity.Cliente;

public class Main {


	public static void main(String[] args) {
	
		
	GestoreOrdiniAcme gestoreOrdini = new GestoreOrdiniAcme();

	
	Cliente c1 = new Cliente("Nicola");  
	Cliente c2 = new Cliente("Ciro");  
	
	
	gestoreOrdini.aggiungiCliente(c1);
	gestoreOrdini.aggiungiCliente(c2);
	
	
	gestoreOrdini.aggiungiSpesa(c1, "S01", 20);
	gestoreOrdini.aggiungiSpesa(c1, "S02", 330);
	gestoreOrdini.aggiungiSpesa(c1, "S03", 30);
	gestoreOrdini.aggiungiSpesa(c1, "S04", 0.5);
	
	
	gestoreOrdini.aggiungiSpesa(c2, "S23", 3.5);
	gestoreOrdini.aggiungiSpesa(c2, "S233", 30);
	
	
	gestoreOrdini.generaReport();
		

	}
	
	//Commento avvenuto push

}
