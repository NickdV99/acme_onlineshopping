package is.acme.entity;

import java.util.ArrayList;

public class Cliente {

	private String nomeUtente;
	private String password; 
	private int numSpese; 
	//listaSpesa è una lista di spese 
	private ArrayList<Spesa> listaSpesa = new ArrayList<Spesa>(); 
	
	
	public Cliente(String nomeUtente, String password, int numSpese) {
		
		this.nomeUtente = nomeUtente; 
		this.password = password; 
		this.numSpese = numSpese; 
	}
	
	public Cliente(String nomeUtente) {
		
		this.nomeUtente = nomeUtente; 
	}


	public String getNomeUtente() {
		return nomeUtente;
	}


	public void setNomeUtente(String nomeUtente) {
		this.nomeUtente = nomeUtente;
	}


	public int getNumSpese() {
		return numSpese;
	}


	public void setNumSpese(int numSpese) {
		this.numSpese = numSpese;
	}
	
	public ArrayList<Spesa> getListaSpesa() {
		return listaSpesa;
	}

	public void setListaSpesa(ArrayList<Spesa> listaSpesa) {
		this.listaSpesa = listaSpesa;
	}

	
	@Override
	public String toString() {
		return "Cliente [nomeUtente=" + nomeUtente + ", numSpese=" + numSpese + "]";
	}
	

	
}
