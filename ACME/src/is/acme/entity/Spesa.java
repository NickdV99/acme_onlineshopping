package is.acme.entity;

public class Spesa {

	private String id;
	private double costoTotale; 
	
	
	
	public Spesa(String id, double costoTotale) {
		
		this.id = id; 
		this.costoTotale = costoTotale; 
	
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public double getCostoTotale() {
		return costoTotale;
	}

	public void setCostoTotale(double costoTotale) {
		this.costoTotale = costoTotale;
	}

	@Override
	public String toString() {
		return "Spesa [id=" + id + ", costoTotale=" + costoTotale + "]";
	}
	
	
	
	

}
